-- set the leader keys at the start such that keybinds are not getting messed up
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

require("zeljkobekcic.lazy_bootstrap")
require("zeljkobekcic.lazy_plugins")
require('zeljkobekcic.options')
require('zeljkobekcic.keymaps')
require('zeljkobekcic.plugin_configs')
-- vim: ts=2 sts=2 sw=2 et
