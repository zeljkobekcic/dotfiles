return {
  'stevearc/oil.nvim',
  config = function()
    require("oil").setup()
    vim.keymap.set("n", "-", function() vim.api.nvim_command("Oil") end, { desc = "Open parent directory" })
  end,
  dependencies = { "nvim-tree/nvim-web-devicons" },
}
