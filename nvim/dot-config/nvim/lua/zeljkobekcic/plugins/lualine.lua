return {
  -- Set lualine as statusline
  'nvim-lualine/lualine.nvim',
  -- enabled = false,
  -- See `:help lualine.txt`
  opts = {
    options = {
      icons_enabled = false,
      theme = 'auto',
      component_separators = '|',
      section_separators = '',
    },
    sections = {
      lualine_c = { { "filename", path = 1 } }
    },
    inactive_sections = {
      lualine_c = { { 'filename', path = 1 } }
    },
  }
}
