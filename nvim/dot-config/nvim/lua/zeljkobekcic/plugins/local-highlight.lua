-- local-highlight highlights the words under the cursor
return {
  'tzachar/local-highlight.nvim',
  opts = {
    -- file_types = {'python', 'cpp'}, -- If this is given only attach to this
    -- OR attach to every filetype except:
    disable_file_types = { 'tex' },
    hlgroup = 'Search',
    cw_hlgroup = nil,
    -- Whether to display highlights in INSERT mode or not
    insert_mode = false,
  }
}
