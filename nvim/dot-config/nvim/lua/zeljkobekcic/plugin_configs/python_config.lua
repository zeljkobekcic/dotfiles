local venvpath = vim.fn.stdpath 'data' .. '/venv'
local pythonbin = venvpath .. '/bin/python'

if not vim.loop.fs_stat(venvpath) then
  vim.fn.system {
    'python3',
    '-m',
    'venv',
    venvpath
  }

  vim.fn.system {
    pythonbin,
    '-m',
    'pip',
    'install',
    'pynvim',
    'debugpy'
  }
end

vim.g.python3_host_prog = pythonbin
