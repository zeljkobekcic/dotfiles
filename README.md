# Dotfiles

My dotfiles contain configuration for:

- zsh
- kitty
- starship 
- neovim (based on <https://github.com/nvim-lua/kickstart.nvim>)
- fonts (NerdFont patched Hack)
- arch linux
- swaywm

## Requirements

GNU Stow is required for installing the configurations.

Here are requirements listed for each component.

## Installation

```shell
git clone git@gitlab.com:zeljkobekcic/dotfiles.git
git submodule update --init --recursive
```


